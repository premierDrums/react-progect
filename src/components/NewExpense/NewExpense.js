import React, { useState } from "react";

import ExpenseForm from "./ExpenseForm";
import "./NewExpense.css";

const NewExpense = (props) => {
  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    props.onAddExpense(expenseData);
    setExpenses(false);
  };
  const [enteredNewExpenses, setExpenses] = useState(false);
  const setNewExpenses = function () {
    setExpenses(true);
  };
  const stopEditingHandlers = function () {
    setExpenses(false);
  };

  return (
    <div className="new-expense">
      {!enteredNewExpenses && (
        <button onClick={setNewExpenses}>add new expense</button>
      )}
      {enteredNewExpenses && (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          onEditing={stopEditingHandlers}
        />
      )}
    </div>
  );
};

export default NewExpense;
